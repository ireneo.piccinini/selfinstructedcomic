
# define structure array (how many squares and balloons)
# define low-limit percentage to validate against englishness

def main()
    # get one or more pages from babel based on structure array
    # load english dictionary (or its link to resource)
    # get one box of balloons for each structure bit
    # if everything passed and returned a full box, render everything

def box(balloons=[])
    # init empty list for balloon strings
    # get balloons
    # validate each balloon's englishness
    # match against low-limit, return nothing if it doesn't pass
    # if passed repeat for next balloon if any
    # return list of balloon strings

def balloon(page='')
    # init empty word list
    # get validated nth word and add to word list
    # strip that from the start and do over until completely parsed
    # remove one letter words from word list (test)
    # return space-joined phrase

def validate(word='')
    # validate whole message string against english dictionary
    # until a match is found, strip a letter from the end and do over
    # if a match is not found, return just the first letter

def englishness(balloon='', page='')
    # strip spaces from balloon string
    # calculate percentage of balloon letters in original page string
    # return percentage

def render(strip=[])
    # just print as plain text dude