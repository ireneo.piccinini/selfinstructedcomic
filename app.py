#!/usr/bin/env python3

import random, os
from math import ceil
from datetime import datetime

# define structure array (how many squares and balloons)
structure = [ [1, 0], [0, 1], [1, 0] ]
# define low-limit percentages to validate against englishness
limit_threshold = 10
low_limits = range(0, 100, limit_threshold)
# define usable characters to randomize (e.g. english alphabet)
charset = "abcdefghijklmnopqrstuvwxyz"
# define page length for each balloon randomization (e.g. 3200)
pagelen = 3200
# define minimum word length to be accepted
wordminlen = 4
# load english dictionary (or its link to resource)
dict_res = open('./dicts/en.txt', 'r')
dictionary = set(line.strip() for line in dict_res)
dict_res.close()
# prepares generated comics folder
comics_folder = './comics_gen/byletters/'
os.makedirs(comics_folder, exist_ok=True)

def main():
    # get randomized page based on structure array
    page = []
    lowlimit = 100
    for i, box in enumerate(structure):
        page.append([])
        if lowlimit <= 0:
            break
        for balloon in box:
            if balloon:
                balloon_text = randomize(charset, pagelen)
                balloon = make_balloon(balloon_text)
                balloon_englishness = get_englishness(balloon, balloon_text)
                balloon_limit = english_limit(balloon_englishness)
                lowlimit = min(lowlimit, balloon_limit)
                if lowlimit <= 0:
                    break
            page[i].append(balloon)
    if lowlimit <= 0:
        return
    # if everything passed and returned a full box, render everything
    render(page, lowlimit)

def randomize(charset=[], length=0):
    # return randomized string based on the given charset
    random_string = ''
    if hasattr(charset, '__iter__') and length > 0:
        charset_len = len(charset)
        charset_list = list(charset)
        random.seed()
        for i in range(length):
            random.shuffle(charset_list)
            random_string += charset_list[0]
    return random_string

def make_balloon(page=''):
    # init empty word list
    word_list = []
    # get validated nth word and add to word list
    page_len = len(page)
    words = page
    while page_len > 0 and len(words) > wordminlen:
        page_len -= 1
        word = validate(words)
        word_len = len(word)
        # remove one letter words from word list (test)
        if word_len > wordminlen:
            word_list.append(word)
        # strip that from the start and do over until completely parsed
        words = words[word_len:]
    # return space-joined phrase
    return ' '.join(word_list)

def validate(word=''):
    # validate whole message string against english dictionary
    found = False
    while len(word) > wordminlen and not found:
        if word in dictionary:
            found = word
            break
        # until a match is found, strip a letter from the end and do over
        word = word[:-1]
    if not found:
        found = 'a' * wordminlen
    # if a match is not found, return just the first letter
    return found

def get_englishness(balloon='', page=''):
    # strip spaces from balloon string
    balloon_string = balloon.replace(' ', '')
    # calculate percentage of balloon letters in original page string
    englishness = len(balloon_string) * 100 / len(page)
    # return percentage
    return ceil(englishness)

def english_limit(balloon_limit=0):
    limit_level = balloon_limit - balloon_limit % limit_threshold
    low_limit = low_limits[round(limit_level / limit_threshold)]
    return low_limit

def render(strip=[], englishness=0):
    if englishness > 0:
        string = ''
        #print("EN > {}%".format(englishness))
        for box in strip:
            char_ord = ord('A')
            for balloon in box:
                char = chr(char_ord)
                char_ord += 1
                if balloon:
                    string +=  "{}: {}\n".format(char, balloon)
            string += "\n"
        filename = datetime.utcnow().strftime('%Y%m%d_%H%M%S_%f.txt')
        filepath = "{}{}/{}".format(comics_folder, str(englishness), filename)
        os.makedirs(os.path.dirname(filepath), exist_ok=True)
        with open(filepath, "w") as f:
            f.write(string)

if __name__ == "__main__":
    main()